#pragma once
#include <string>
#ifndef Tetromino_h
#define Tetromino_h

class Tetromino
{	
private:
	void Build();
public:
	std::wstring tetromino[7];
	Tetromino();
};

#endif 